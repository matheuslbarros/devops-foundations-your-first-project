FROM nginx:alpine
LABEL maintainer="matheuslbarros@gmail.com"

COPY website /website
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
